#!/usr/bin/env bash

REPO=avskit
REPO_URL=https://ucic-1@bitbucket.org/ucic-1/$REPO.git

echo "Starting AVS Kit install"

cd ~
echo "Downloading..."
git clone --quiet $REPO_URL > /dev/null
echo "Copying build to home directory"
mv $REPO/build/* ~
echo "Cleaning up..."
rm -rf $REPO
chmod +x setup-avs.sh
echo "Installing..."
bash setup-avs.sh
